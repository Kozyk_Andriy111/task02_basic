package com.epam;

import java.util.Scanner;

/**
 * Class Interval.
 * Performs the first part of task02_basic (work with interval)
 */
public class Interval {
    /**array - is array which contain an interval.*/
    private int[] array;
    /**minValue saves start value of interval.*/
    private int minValue;
    /**maxValue saves last value of interval.*/
    private int maxValue;
    /**sizeArray saves size of array.*/
    private int sizeArray;

    /**
     * Interval method reads the data entered by the user and forms an interval.
     */
    public final void intervalBuild() {
        Scanner in = new Scanner(System.in);

        System.out.println("Введiть межi iнтервалу:");
        System.out.print("Вiд: ");
        minValue = in.nextInt();
        System.out.print("До: ");
        maxValue = in.nextInt();

        System.out.println("Введений iнтервал: ["
                + minValue + ";" + maxValue + "]");

        sizeArray = maxValue - minValue + 1;

        array = new int[sizeArray];

        for (int i = 0; i < sizeArray; i++) {
            array[i] = minValue;
            minValue++;
        }
    }

    /**
     * PrintOddNumber method prints odd numbers from start to
     * the end of interval and even from end to start.
     */
    public final void printOddNumber() {
        System.out.println("Непарнi числа вiд початку до кiнця: ");
        for (int i = 0; i < sizeArray; i++) {
            if (array[i] % 2 == 1) {
                System.out.print(array[i] + " ");
            }
        }
        System.out.println("\nНепарнi числа вiд кiнця до початку: ");
        for (int i = sizeArray - 1; i >= 0; i--) {
            if (array[i] % 2 == 1) {
                System.out.print(array[i] + " ");
            }
        }
    }

    /**
     * PrintSumNumber method prints the sum of odd and even numbers.
     */
    public final void printSumNumber() {
        int sumOdd = 0;
        int sumEven = 0;
        for (int i = 0; i < sizeArray; i++) {
            if (array[i] % 2 == 1) {
                sumOdd += array[i];
            } else {
                sumEven += array[i];
            }
        }
        System.out.println("\nСума непарних чисел: " + sumOdd);
        System.out.println("Сума парних чисел: " + sumEven);
    }
}
