package com.epam;
/**
 * Main class.
 *
 * @author Andriy Kozyk a.k.t.k.o.k.p@gmail.com
 * @version 1.0
 * @since 2019-11-08
 */
public class Application {
    /**
     * This is main function of project.
     * @param args The main param args.
     * */
    public static void main(final String[] args) {
        /**
         * App1 performs interval task
         */
        Interval app1 = new Interval();
        app1.intervalBuild();
        app1.printOddNumber();
        app1.printSumNumber();
        /**
         * App2 performs Fibonacci numbers task
         */
        Fibonacci app2 = new Fibonacci();
        app2.buildFibonacci();
        app2.percentage();
    }
}
