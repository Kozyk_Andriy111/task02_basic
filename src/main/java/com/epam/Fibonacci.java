package com.epam;

import java.util.Scanner;

/**
 * Class Fibonacci.
 * Performs the second part of task02_basic (work with Fibonacci numbers).
 */
public class Fibonacci {
    /**array is array of set Fibonacci numbers.*/
    private int[] array;
    /** sizeOfSet is size of set Fibonacci numbers(N).*/
    private int sizeOfSet;
    /** biggestOddNumber is the biggest odd
     *  number of Fibonacci numbers(F1).*/
    private int biggestOddNumber;
    /** biggestEvenNumber is the biggest even
     * number of Fibonacci numbers(F2).*/
    private int biggestEvenNumber;

    /**
     * BuildFibonacci method build Fibonacci numbers:F1 will
     * be the biggest odd number and F2 – the biggest even
     * number, user can enter the size of set (N).
     */
    public final void buildFibonacci() {
        Scanner in = new Scanner(System.in);

        /** stopF1 = false, when F1 write*/
        boolean stopF1 = true;
        /** stopF2 = false, when F2 write*/
        boolean stopF2 = true;
        System.out.print("Введiть розмiр набору: ");
        sizeOfSet = in.nextInt();
        array = new int[sizeOfSet];
        array[0] = 0;
        array[1] = 1;
        for (int i = 2; i < sizeOfSet; i++) {
            array[i] = array[i - 1] + array[i - 2];
        }
        System.out.println("Сформований набiр Фiбоначi:");
        for (int i = 0; i < sizeOfSet; i++) {
            System.out.print(array[i] + " ");
        }
        for (int i = sizeOfSet - 1; i >= 0; i--) {
            if ((array[i] % 2 == 1) && stopF1) {
                biggestOddNumber = array[i];
                stopF1 = false;
            } else if ((array[i] % 2 == 0) && stopF2) {
                biggestEvenNumber = array[i];
                stopF2 = false;
            }
        }
        System.out.println("\nНайбiльше непарне "
                + "число з послiдовностi Фібоначi: " + biggestOddNumber);
        System.out.println("Найбiльше парне число "
                + "з послiдовностi Фiбоначi: " + biggestEvenNumber);
    }

    /**
     * Percentage method prints percentage of odd and even Fibonacci numbers.
     */
    public final void percentage() {
        final int const100 = 100;
        int percentageOdd;
        int percentageEven;
        int sizeOfOdd = 0;
        int sizeOfEven = 0;

        for (int i = 0; i < sizeOfSet; i++) {
            if (array[i] % 2 == 1) {
                sizeOfOdd++;
            } else {
                sizeOfEven++;
            }
        }
        percentageOdd = sizeOfOdd * const100 / sizeOfSet;
        percentageEven = sizeOfEven * const100 / sizeOfSet;

        System.out.println("Непарнi числа: " + percentageOdd + "%");
        System.out.println("Парнi числа: " + percentageEven + "%");
    }
}
